package com.tharun.guuiii

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tharun.guuiii.ui.theme.GuuiiiTheme


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GuuiiiTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background,
                ) { Body() }
            }
        }
    }
}

@Composable fun Body() {
    Column(
        Modifier.padding(20.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        var count by remember { mutableStateOf(0) }
        var display by remember { mutableStateOf(false) }
        if (display) {
            Card(elevation = 5.dp) { Text(count.toString(), Modifier.padding(20.dp)) }
            Spacer(Modifier.padding(10.dp))
        }

        Row {
            Button(
                onClick = {
                    count++
                    display = true
                },
                colors = ButtonDefaults.buttonColors(
                    contentColor = Color(0xFF000000)),
            ) {
                Text("Click Me!")
            }
            if (count != 0) {
                Spacer(Modifier.padding(10.dp))
                Button(
                    onClick = {
                        count = 0
                        display = false
                    },
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color(0xFFFF0000)),
                ) {
                    Text("  Reset  ")
                }
            }
        }
    }

}

@Preview(
    name = "Preview", showBackground = true)
@Composable fun LightTheme() = GuuiiiTheme { Body() }
