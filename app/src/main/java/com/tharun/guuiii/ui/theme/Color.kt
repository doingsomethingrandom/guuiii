package com.tharun.guuiii.ui.theme

import androidx.compose.ui.graphics.Color

val Teal100 = Color(0xFF9FFFFF)
val Teal200 = Color(0xFF00FFFF)
val Teal500 = Color(0xFF03DAC5)
val Teal700 = Color(0xFF018786)
