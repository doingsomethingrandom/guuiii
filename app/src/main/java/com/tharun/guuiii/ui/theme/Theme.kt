package com.tharun.guuiii.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

@Composable
fun GuuiiiTheme(content: @Composable () -> Unit) {
    MaterialTheme(
        colors = lightColors(
            primary = Teal200,
            primaryVariant = Teal500,
            secondary = Teal100,
        ),
        typography = Typography,
        shapes = Shapes,
        content = content,
    )
}